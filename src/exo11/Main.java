package exo11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class Main {

	public static void main(String[] args) {
		
		Person p1 = new Person();
		Person p2 = new Person();
		Person p3 = new Person();
		Person p4 = new Person();
		
		p1.setFirstName("nelson");
		p1.setLastName("mandela");
		p1.setAge(22);
		
		p2.setFirstName("graca");
		p2.setLastName("mandela");
		p2.setAge(42);
		
		p3.setFirstName("");
		p3.setLastName("");
		p3.setAge(0);
		
		p4.setFirstName("martin");
		p4.setLastName("luther");
		p4.setAge(42);
		
		//List of Person
		List<Person> personnes = new ArrayList<Person>();
		personnes.add(p1);
		personnes.add(p2);
		personnes.add(p3);
		personnes.add(p4);

		
		/****QUESTION 1****/
		Comparator <String> cLen = (String s1, String s2) -> s1.compareTo(s2);
		System.out.println("/****QUESTION 1****/");
		System.out.println(cLen.compare("lutin", "lut"));

		/****QUESTION 2****/
		Comparator <Person> cPersonLast = (Person per1, Person per2) -> per1.getLastName().compareToIgnoreCase(per2.getLastName());
		System.out.println("/****QUESTION 2****/");
		System.out.println(cPersonLast.compare(p1, p2));
		
		/****QUESTION 3****/
		Comparator <Person> cPersonFirst = (Person per1, Person per2) -> per1.getFirstName().compareToIgnoreCase(per2.getFirstName());
		//System.out.println(cPersonFirst.compare(p1, p2));
		
		Comparator <Person> cPersonLastFirst = cPersonLast.thenComparing(cPersonFirst);
		System.out.println("/****QUESTION 3****/");
		System.out.println(cPersonLastFirst.compare(p1, p2));
		
		/****QUESTION 4****/
		Comparator <Person> cPersonFisrtLast = cPersonLast.thenComparing(cPersonFirst.reversed()).reversed();
		System.out.println("/****QUESTION 4****/");
		System.out.println(cPersonFisrtLast.compare(p1, p2));
		
		//sort of list :
		System.out.println("/****SORT OF LIST****/");
		System.out.println("List 'personnes' before sorting : " + personnes);
		personnes.sort(Comparator.nullsLast(cPersonLastFirst));
		System.out.println("List 'personnes' after sorting : " + personnes);

	}

}
