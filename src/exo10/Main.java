package exo10;

import java.util.function.Function;
import java.util.function.BiFunction;

public class Main {
	public static void main(String[] args) {
		/****QUESTION 1****/
		Function <String, String> fStrMaj = (String s) -> s.toUpperCase();
		System.out.println("/****QUESTION 1****/");
		System.out.println(fStrMaj.apply("bonjour"));

		/****QUESTION 2****/
		Function <String, String> fStrSame = (String s) -> s;
		System.out.println("/****QUESTION 2****/");
		System.out.println(fStrSame.apply("bonjour"));

		/****QUESTION 3****/
		Function <String, Integer> fStrLen = (String s) -> s.length();
		System.out.println("/****QUESTION 3****/");
		System.out.println(fStrLen.apply("bonjour"));

		/****QUESTION 4****/
		Function <String, String> fBrackets  = (String s) -> "(" + s + ")";
		System.out.println("/****QUESTION 4****/");
		System.out.println(fBrackets.apply("bonjour"));

		/****QUESTION 5****/
		BiFunction <String, String, Integer> biFStrPos = (String s1, String s2) -> {
			if(s1.contains(s2))
				return s1.indexOf(s2);
			else 
				return -1;
		};
		System.out.println("/****QUESTION 5****/");
		System.out.println(biFStrPos.apply("bonjour","lecteur bonjour a vous"));

		/****QUESTION 6****/
		Function <String, Integer> fStrPos = (String s) -> "abcdeghi".indexOf(s);
		System.out.println("/****QUESTION 6****/");
		System.out.println(fStrPos.apply("bonjour"));
	
	}
}
