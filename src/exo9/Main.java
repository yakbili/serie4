package exo9;

import java.util.function.Predicate;

public class Main {
	public static void main(String[] args) {
		/****QUESTION 1****/	
		Predicate <String> pLenSupQuatre = (String s) -> s.length()>4;
		System.out.println("/****QUESTION 1****/");
		System.out.println("Longueur de 'bonjour' > 4 ? " + pLenSupQuatre.test("bonjour"));
		
		/****QUESTION 2****/
		Predicate <String> pEstVide = (String s) -> !(s.isEmpty());
		System.out.println("/****QUESTION 2****/");
		System.out.println("La chaine 'vide' est-elle non vide ? " + pEstVide.test(""));

		/****QUESTION 3****/
		Predicate <String> pStartJ = (String s) -> s.substring(0,1).matches("J");
		System.out.println("/****QUESTION 3****/");
		System.out.println("Le prenom 'Jean' commence par un 'J' ? " + pStartJ.test("Jean"));

		/****QUESTION 4****/
		Predicate <String> pLenCinq = (String s) -> s.length()==5;
		System.out.println("/****QUESTION 4****/");
		System.out.println("Longueur de 'bonjour' = 5 ? " + pLenCinq.test("bonjour"));

		/****QUESTION 5****/
		Predicate <String> pSupQuatreStartJ = (String s) -> s.substring(0, 1).matches("J") && s.length()>4;
		System.out.println("/****QUESTION 5****/");
		System.out.println("'Jean' strat with a 'J' and its length is > 4 ? " + pSupQuatreStartJ.test("Jean-Baptiste"));

		}
	}

